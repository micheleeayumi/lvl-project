Before do
    Capybara.page.current_window.resize_to(1000, 660)
end

After do |scenario|
    scenario_name = scenario.name.gsub(/[^A-Za-z0-9]/, '')
    scenario_name = scenario_name.gsub(' ','_').downcase!
    screenshot = "log/#{scenario_name}.png"
    page.save_screenshot(screenshot)
end