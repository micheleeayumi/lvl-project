Dir[File.join(File.dirname(__FILE__), "../pages/*_page.rb")].each { |file| require file }

module Pages
  def home
    @home ||= HomePage.new
  end

  def accessories
    @accessories ||= AccessoriesPage.new
  end

  def rain_cover
    @rain_cover ||= RainCoverPage.new
  end

  def cart
    @cart ||= CartPage.new
  end 
end