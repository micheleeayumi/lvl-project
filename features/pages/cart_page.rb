class CartPage < SitePrism::Page
    set_url '/cart/'

    element :quantity, ".qty"
end