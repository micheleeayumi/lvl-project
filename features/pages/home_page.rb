class HomePage < SitePrism::Page
    set_url '/'

    element :carrinho, "#livelo-cart-anchor-toggle"
    element :qtd_carrinho, "#livelo-cart-anchor-toggle > span > span.x-anchor-text > span > span"
    element :cart, "a[href='https://livelo.com/cart/']"
end