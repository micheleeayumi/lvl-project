class RainCoverPage < SitePrism::Page
    set_url '/product/rain-cover/'

    element :accessory_kit, '.tg-item-media-holder > a[href="https://livelo.com/product/accessory-kit/"]'
    element :rain_cover, '.tg-item-media-inner > a[href="https://livelo.com/product/rain-cover/"]'
    element :granite_xplus, '.tg-item-media-inner > a[href="https://livelo.com/product/granit-xplus-540/"]'
    element :abus_city_chain, '.tg-item-media-inner > a[href="https://livelo.com/product/abus-city-chain-1060/"]'
    element :add_cart_button, 'button[name="add-to-cart"]'

    def click_add_cart
        add_cart_button.click
    end
end