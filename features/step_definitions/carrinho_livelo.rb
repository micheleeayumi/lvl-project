Dado('que eu deseje comprar um Rain cover') do
    rain_cover.load
end

Quando('adiciono o Rain cover no carrinho') do
    rain_cover.add_cart_button.hover
    rain_cover.click_add_cart
end

Então('devo ter um item adicionado no carrinho') do
    expect(home.qtd_carrinho).to have_content('1')
end

Dado('que adicionei o Rain cover no carrinho') do
    rain_cover.load
    rain_cover.add_cart_button.hover
    rain_cover.click_add_cart
end

Quando('acesso o carrinho') do
    home.cart.click()
end

Então('devo ver o Rain cover') do
    expect(cart).to have_link("Rain cover", :href=>"https://livelo.com/product/rain-cover/")
end

Então('com {string} unidade para comprar') do |number|
    expect(cart).to have_field(cart.quantity, :maximum => 3, :minimum => 0, with: number)
end

def scroll_to_element(elem)
    page.execute_script('arguments[0].scrollIntoView();', elem)
end