Dado('que eu esteja na pagina da livelo') do
  home.load
end
  
Entao('devo ver a pagina home') do
  expect(home).to have_title "Home - Livelo"
end

Entao('os redirecionamentos para as outras paginas') do
  expect(home).to have_link('Livelo #1', :href=> 'https://livelo.com/product/livelo-1/')
  expect(home).to have_link('Accessories', :href=> 'https://livelo.com/accessories/')
end
  
Dado('que eu esteja buscando por um acessorio da livelo') do
  accessories.load
end

Então('devo ver a pagina de acessorios') do
  expect(accessories).to have_title "Accessories - Livelo"
end

Então('os produtos dispostos para compra') do
  expect(accessories).to have_link("Accessory kit", :href=>"https://livelo.com/product/accessory-kit/")
  expect(accessories).to have_link("Rain cover", :href=>"https://livelo.com/product/rain-cover/")
  expect(accessories).to have_link("Granite xPlus 540", :href=>"https://livelo.com/product/granit-xplus-540/")
  expect(accessories).to have_link("Abus City Chain 1060 140cm", :href=>"https://livelo.com/product/abus-city-chain-1060/")
end