#language: pt
Funcionalidade: Acesso Livelo
    COMO um cliente da loja
    QUERO acessar o site da Livelo
    DE FORMA QUE eu consiga visualizar os produtos da mesma

Cenario: Acessar a home page e com os redirecionamentos para as paginas
    Dado que eu esteja na pagina da livelo
    Entao devo ver a pagina home
    E os redirecionamentos para as outras paginas

Cenario: Acessar a pagina de acessórios
    Dado que eu esteja buscando por um acessorio da livelo
    Então devo ver a pagina de acessorios
    E os produtos dispostos para compra