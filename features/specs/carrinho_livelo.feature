#language: pt
Funcionalidade: Colocar um produto no carrinho
    COMO um cliente da loja
    QUERO comprar um produto na Livelo
    DE FORMA QUE eu consiga obter o produto em sucesso

Cenario: Adicionar Rain Cover no carrinho
    Dado que eu deseje comprar um Rain cover
    Quando adiciono o Rain cover no carrinho
    Então devo ter um item adicionado no carrinho

Cenario: Acesso ao carrinho
    Dado que adicionei o Rain cover no carrinho
    Quando acesso o carrinho
    Então devo ver o Rain cover
    E com "1" unidade para comprar