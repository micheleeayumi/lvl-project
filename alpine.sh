echo "http://dl-4.alpinelinux.org/alpine/v3.4/main" >> /etc/apk/repositories && \
echo "http://dl-4.alpinelinux.org/alpine/v3.4/community" >> /etc/apk/repositories

apk add --update \
    build-base \
    libxml2-dev \
    libxslt-dev \
    curl unzip libexif udev chromium chromium-chromedriver xvfb xorg-server dbus ttf-freefont mesa-dri-swrast \
    && rm -rf /var/cache/apk/*
    
bundle config build.nokogiri --use-system-libraries